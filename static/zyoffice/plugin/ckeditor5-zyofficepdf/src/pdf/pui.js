import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';
import icon from '@ckeditor/ckeditor5-zyofficepdf/theme/icons/p.svg';

export default class pdfUI extends Plugin {
	/**
	 * @inheritDoc
	 */
	init() {
		const editor = this.editor;
		const t = editor.t;

		// Setup `imageUpload` button.
		editor.ui.componentFactory.add( 'ZyOfficePdf', locale => {
			const button = new ButtonView( locale );

			button.isEnabled = true;
			button.label = t( '导入PDF文档' );
			button.icon = icon;
			//button.keystroke = relKeystroke;
			//button.class="relFileSel";
			button.tooltip = true;

			// Show the panel on button click.
			this.listenTo( button, 'execute', () => {
				zyOffice.getInstance().api.openPdf();
			});

			return button;
		} );
	}
}
