import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import wCommand from '@ckeditor/ckeditor5-zyofficepdf/src/pdf/command';

/**
 * Image upload editing plugin.
 *
 * @extends module:core/plugin~Plugin
 */
export default class pdfEdit extends Plugin {

	/**
	 * @inheritDoc
	 */
	init() {
		const editor = this.editor;
		//editor.model.schema.extend( '$text', { allowAttributes: 'linkHref' } );
		// Create bold command.
		editor.commands.add( 'ZyOfficePdf', new wCommand(editor) );
	}

}
