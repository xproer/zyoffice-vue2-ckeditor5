import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import Widget from '@ckeditor/ckeditor5-widget/src/widget';
import ui from '@ckeditor/ckeditor5-zyoffice/src/office/zui';
import edit from '@ckeditor/ckeditor5-zyoffice/src/office/zedit';

export default class ZyOffice extends Plugin {
	/**
	 * 定义插件名称
	 */
	static get pluginName() {
		return 'ZyOffice';
	}

	/**
	 * 实现插件的关键接口
	 * 主要实现功能以及注册command ，RelFileSelUI主要是toolbar上按钮定义等
	 */
	static get requires() {
		return [edit,ui,Widget];
	}
}