import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import wCommand from '@ckeditor/ckeditor5-zyoffice/src/office/command';

/**
 * Image upload editing plugin.
 *
 * @extends module:core/plugin~Plugin
 */
export default class officeEdit extends Plugin {

	/**
	 * @inheritDoc
	 */
	init() {
		const editor = this.editor;
		//editor.model.schema.extend( '$text', { allowAttributes: 'linkHref' } );
		// Create bold command.
		editor.commands.add( 'ZyOffice', new wCommand(editor) );
	}

}
