/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md.
 */


 import Command from '@ckeditor/ckeditor5-core/src/command';
 /**
  * @module image/imageupload/imageuploadcommand
  */
 
 /**
  * Image upload command.
  *
  * @extends module:core/command~Command
  */
 var seperator = '、';
 export default class wordexportCommand extends Command {
     refresh() {
         this.isEnabled = true;
     }
 
     execute({backContent, attachmentId}) {
 
         const editor = this.editor;
         const model = editor.model;
         const document = model.document;
         const selection = document.selection;
 
         model.change(writer => {
             var viewFragment = editor.data.processor.toView(backContent.join(seperator));
             var modelFragment = editor.data.toModel(viewFragment);
             editor.model.insertContent(modelFragment, editor.model.document.selection);
 
             if(attachmentId!=null){
             }
 
         });
 
     }
 }