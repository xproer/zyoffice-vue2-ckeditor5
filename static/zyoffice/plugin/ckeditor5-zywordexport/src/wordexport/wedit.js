import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import wCommand from '@ckeditor/ckeditor5-zywordexport/src/wordexport/command';

/**
 * Image upload editing plugin.
 *
 * @extends module:core/plugin~Plugin
 */
export default class wordexportEdit extends Plugin {

	/**
	 * @inheritDoc
	 */
	init() {
		const editor = this.editor;
		//editor.model.schema.extend( '$text', { allowAttributes: 'linkHref' } );
		// Create bold command.
		editor.commands.add( 'ZyWordExport', new wCommand(editor) );
	}

}
