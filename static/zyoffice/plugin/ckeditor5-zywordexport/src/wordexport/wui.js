import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';
import icon from '@ckeditor/ckeditor5-zywordexport/theme/icons/w.svg';

export default class wordexportUI extends Plugin {
	/**
	 * @inheritDoc
	 */
	init() {
		const editor = this.editor;
		const t = editor.t;

		// Setup `imageUpload` button.
		editor.ui.componentFactory.add( 'ZyWordExport', locale => {
			const button = new ButtonView( locale );

			button.isEnabled = true;
			button.label = t( '导出Word文档（docx格式）' );
			button.icon = icon;
			//button.keystroke = relKeystroke;
			//button.class="relFileSel";
			button.tooltip = true;

			// Show the panel on button click.
			this.listenTo( button, 'execute', () => {
				zyOffice.getInstance().api.exportWord();
			});

			return button;
		} );
	}
}
